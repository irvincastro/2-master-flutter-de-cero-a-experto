import 'package:flutter/material.dart';
import 'package:flutterchat_app/src/widgets/app_button.dart';
import 'package:flutterchat_app/src/widgets/app_icon.dart';
import 'package:flutterchat_app/src/widgets/app_textfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutterchat_app/src/services/authentication.dart';

class RegistrationScreen extends StatefulWidget {
  static const String routeName = '/registration';
  @override
  _RegistrationScreenState createState() => new _RegistrationScreenState();
 }
class _RegistrationScreenState extends State<RegistrationScreen> {
  String _email;
  String _password;
  FocusNode _focusNode;
  TextEditingController _emailController;
  TextEditingController _passwordController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _focusNode = FocusNode();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _focusNode.dispose();
    _emailController.dispose();
    _passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            AppIcon(),
            SizedBox(height: 48.0),
            AppTextField(inputText: "Ingresar email",
            focusNode: _focusNode,
            controller: _emailController,
            onChanged: (value) {_email = value;
            print("Email $_email");
            }),
            SizedBox(height: 8.0),
            AppTextField(
              controller: _passwordController,
            inputText: "Ingresar contraseña",
            obscureText: true,
            onChanged: (value) {_password = value;
            print("Password $_password");
            }),
            SizedBox(height: 24.0),
            AppButton(
              color: Colors.blueAccent,
              onPressed: () async {
                var newUser = await Authentication().createUser(email: _email, password: _password);
                if(newUser != null){
                  Navigator.pushNamed(context, '/chat');
                }
                _emailController.text = "";
                _passwordController.text = "";
                FocusScope.of(context).requestFocus(_focusNode);
              },
              name: "Registrarse"
            )
          ],
        ),
      ),
    );
  }
}