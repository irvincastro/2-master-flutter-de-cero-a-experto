import 'package:flutter/material.dart';
import 'package:flutterchat_app/screens/welcome_screen.dart';

void main() {
  runApp(
    MaterialApp(
      home: WelcomeScreen(),
      theme: ThemeData(
        textTheme: TextTheme(
          //Propiedad body1: OBSOLETA.
          bodyText2: TextStyle(color: Colors.black45)
        )
      )
    )
  );
}