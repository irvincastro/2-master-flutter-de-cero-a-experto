import 'package:flutter/material.dart';
import 'package:flutterchat_app/src/mixins/validation_mixins.dart';
import 'package:flutterchat_app/src/services/authentication.dart';
import 'package:flutterchat_app/src/widgets/app_button.dart';
import 'package:flutterchat_app/src/widgets/app_icon.dart';
import 'package:flutterchat_app/src/widgets/app_textfield.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class LoginScreen extends StatefulWidget {
  static const String routeName = '/login';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with ValidationMixins{
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  FocusNode _focusNode;
  bool showSpinner = false;
  bool _autoValidate = false;
  //Otra forma de inicializar:
  //FocusNode _focusNode = FocusNode();
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _focusNode.dispose();
    _emailController.dispose();
    _passwordController.dispose();
  }

  void setSpinnerStatus(bool status){
    setState(() {
      showSpinner = status;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: showSpinner, 
        child: Form(
          key: _formkey,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                AppIcon(),
                SizedBox(height: 48.0),
                _emailField(),
                SizedBox(height: 8.0),
                _passwordField(),
                SizedBox(height: 23.0),
                _submitButton()
              ]
            )
          )
        )
      )
    );
  }

  Widget _emailField(){
    return AppTextField(
                focusNode: _focusNode,
                autoValidate: _autoValidate,
                controller: _emailController,
                validator: validateEmail,
                inputText: "Ingresar email",
                onChanged: (value) { }
                );
  }
  Widget _passwordField(){
    return AppTextField(
      controller: _passwordController,
      autoValidate: _autoValidate,
      inputText: "Ingresar contraseña",
      validator: validatePassword, //Al parecer el parámetro se envía automáticamente
      obscureText: true,
      onChanged: (value) { }
    );
  }
  Widget _submitButton(){
    return AppButton(
      color: Colors.blueAccent,
      onPressed: () async {
        if(_formkey.currentState.validate()){
          setSpinnerStatus(true);
          var user = await Authentication().loginUser(email: _emailController.text, password: _passwordController.text);
          if(user != null){
            Navigator.pushNamed(context, '/chat');
          }
          FocusScope.of(context).requestFocus(_focusNode);
          _emailController.text = "";
          _passwordController.text = "";
          setSpinnerStatus(false);
        }else{
          setState(() => _autoValidate = true);
        }
      },
      name: 'Log in'
    );
  }
}