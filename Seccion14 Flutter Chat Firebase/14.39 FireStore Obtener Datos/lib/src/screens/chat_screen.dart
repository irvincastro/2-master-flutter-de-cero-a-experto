import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutterchat_app/src/services/authentication.dart';
import 'package:flutterchat_app/src/services/message_service.dart';

class ChatScreen extends StatefulWidget {
  static const String routeName = '/chat';
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final auth = FirebaseAuth.instance;
  //OBSOLETO:
  //FirebaseUser loggedInUser;
  User loggedInUser;

  TextEditingController _messageController = TextEditingController();
  InputDecoration _messageTextFieldDecotarion = InputDecoration(
    contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
    hintText: 'Ingresar su mensaje aquí....',
    border: InputBorder.none
  );
  BoxDecoration _messageContainerDecoration = BoxDecoration(
    border: Border(
      top: BorderSide(color: Colors.lightBlueAccent, width: 2.0)
    )
  );

  TextStyle _sendButtonStyle = TextStyle(
    color: Colors.lightBlueAccent,
    fontWeight: FontWeight.bold,
    fontSize: 18.0
  );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrentUser();
    _getMessage();
  }
  void getCurrentUser() async{
      var user = await Authentication().getCurrentUser();
      if(user != null){
        loggedInUser = user;
        print(loggedInUser.email);
      }
  }
  
  void _getMessage() async{
    final messages = await MessageService().getMessage();
    for(var message in messages.docs){
      print(message.data());
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chat Screen"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.power_settings_new),
            onPressed: () {
              Authentication().signOut();
              Navigator.pop(context);
            }
          )
        ],
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              decoration: _messageContainerDecoration,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      decoration: _messageTextFieldDecotarion,
                      controller: _messageController,
                    )
                  ),
                  FlatButton(
                    child: Text("Enviar", style: _sendButtonStyle),
                    onPressed: (){
                      MessageService().save(collectionName: "messages", collectionValues:{
                        'value': _messageController.text,
                        'sender': loggedInUser.email
                      });
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}