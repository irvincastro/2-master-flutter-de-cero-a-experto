import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutterchat_app/src/services/authentication.dart';
import 'package:flutterchat_app/src/services/message_service.dart';

class ChatScreen extends StatefulWidget {
  static const String routeName = '/chat';
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final auth = FirebaseAuth.instance;
  //OBSOLETO:
  //FirebaseUser loggedInUser;
  User loggedInUser;

  TextEditingController _messageController = TextEditingController();
  InputDecoration _messageTextFieldDecotarion = InputDecoration(
    contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
    hintText: 'Ingresar su mensaje aquí....',
    border: InputBorder.none
  );
  BoxDecoration _messageContainerDecoration = BoxDecoration(
    border: Border(
      top: BorderSide(color: Colors.lightBlueAccent, width: 2.0)
    )
  );

  TextStyle _sendButtonStyle = TextStyle(
    color: Colors.lightBlueAccent,
    fontWeight: FontWeight.bold,
    fontSize: 18.0
  );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrentUser();
    _getMessage();
  }
  void getCurrentUser() async{
      var user = await Authentication().getCurrentUser();
      if(user != null){
        loggedInUser = user;
        print(loggedInUser.email);
      }
  }
  
  void _getMessage() async{
    await for(var snapshot in MessageService().getMessageStream()){
      for(var message in snapshot.docs){
        print(message.data());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chat Screen"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.power_settings_new),
            onPressed: () {
              Authentication().signOut();
              Navigator.pop(context);
            }
          )
        ],
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            StreamBuilder(
              //Para escuchar los datos nuevos del stream
              stream: MessageService().getMessageStream(),
              //El builder se gatilla y ejecuta cada que hay un nuevo dato en el stream 
              builder: (context, snapshot){
                if(snapshot.hasData){
                  var messages = snapshot.data.documents;
                  List<ChatItem> chatItems = [];
                  for(var message in messages){
                    //.data() debe llevar paréntesis
                    final messageValue = message.data()["value"];
                    final messageSender = message.data()["sender"];
                    chatItems.add(ChatItem(message: messageValue, sender: messageSender));
                  }

                  return Flexible(
                      child: ListView(
                        children: chatItems
                    )
                  );

                }
              }
            ),
            Container(
              decoration: _messageContainerDecoration,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      decoration: _messageTextFieldDecotarion,
                      controller: _messageController,
                    )
                  ),
                  FlatButton(
                    child: Text("Enviar", style: _sendButtonStyle),
                    onPressed: (){
                      MessageService().save(collectionName: "messages", collectionValues:{
                        'value': _messageController.text,
                        'sender': loggedInUser.email
                      });
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ChatItem extends StatelessWidget {
  final String sender;
  final String message;
  ChatItem({this.sender, this.message});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(sender, style: TextStyle(fontSize: 15.0, color: Colors.black54)),
          Material(
            borderRadius: BorderRadius.circular(30.0),
            color: Colors.lightBlueAccent,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: Text(message, style: TextStyle(fontSize: 16.0, color: Colors.white))
            )
          )
        ],
      ),
    );
  }
}