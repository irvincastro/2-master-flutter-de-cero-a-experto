import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutterchat_app/src/screens/chat_screen.dart';
import 'package:flutterchat_app/src/screens/login_screen.dart';
import 'package:flutterchat_app/src/screens/registration_screen.dart';
import 'package:flutterchat_app/src/screens/welcome_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    MaterialApp(
      home: WelcomeScreen(),
      theme: ThemeData(
        textTheme: TextTheme(
          //Propiedad body1: OBSOLETA.
          bodyText2: TextStyle(color: Colors.black45)
        )
      ),
      initialRoute: WelcomeScreen.routeName,
      routes: <String, WidgetBuilder>{
        LoginScreen.routeName: (BuildContext context) => LoginScreen(),
        WelcomeScreen.routeName: (BuildContext context) => WelcomeScreen(),
        RegistrationScreen.routeName: (BuildContext context) => RegistrationScreen(),
        ChatScreen.routeName: (BuildContext context) => ChatScreen()
      }
    )
  );
}