import 'package:flutter/material.dart';
import 'package:flutterchat_app/screens/welcome_screen.dart';

void main() {
  runApp(
    MaterialApp(
      home: WelcomeScreen(),
      theme: ThemeData(
        textTheme: TextTheme(
          bodyText1: TextStyle(color: Colors.black45)
        )
      )
    )
  );
}