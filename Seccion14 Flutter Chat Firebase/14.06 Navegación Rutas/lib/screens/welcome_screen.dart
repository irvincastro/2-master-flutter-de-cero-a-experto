import 'package:flutter/material.dart';

class WelcomeScreen extends StatefulWidget {
  static const String routeName = '';
  @override
  _WelcomeScreenState createState() => new _WelcomeScreenState();
 }
class _WelcomeScreenState extends State<WelcomeScreen> {

  Widget getButton(MaterialAccentColor color, String name, VoidCallback onPresssed){
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        color: color,
        borderRadius: BorderRadius.circular(30.0),
        elevation: 5.0,
        child: SizedBox(
          height: 43.0,
          child: FlatButton(
            child: Text(name),
            onPressed: onPresssed
          )
        )
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 25.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              children: <Widget>[
                Image.asset('images/logo.png'),
                Text('Flutter Chat',
                  style: TextStyle(
                    fontSize: 45.0,
                    fontWeight: FontWeight.w700
                  )
                )
              ]
            ),
            SizedBox(height: 48.0),
            getButton(
              Colors.lightBlueAccent,
              "Log in",
              () {Navigator.pushNamed(context, '/login');}
            ),
            getButton(
              Colors.blueAccent,
              "Registrarse",
              () {}
            )
          ]
        )
      )
    );   
  }
}