import 'package:flutter/material.dart';
import 'package:form_app/src/mixins/validation_mixins.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with ValidationMixins {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            emailField(),
            passwordField(),
            Container(margin: EdgeInsets.only(top: 25.0)),
            submitField()
          ]
        )
      )
    );
  }

  Widget emailField(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Email',
        hintText: 'you@example.com'
      ),
      validator: validateEmail,
      onSaved: (String value){
        print("Save" + value);
      }
    );
  }

  Widget passwordField(){
    return Container(
      margin: EdgeInsets.only(bottom: 25.0),
      child:
    TextFormField(
      obscureText: false,
      decoration: InputDecoration(
        labelText: 'Contraseña',
        hintText: 'Contraseña'
      ),
      validator: validatePassword,
      onSaved: (String value){
        print("Save $value");
      }
    )
    );
  }

  Widget submitField(){
    return RaisedButton(
      child: Text("Enviar"),
      onPressed: (){
        if(formKey.currentState.validate()){
          formKey.currentState.save();
        }        
      }
    );
  }
}