import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        child: Column(
          children: <Widget>[
            emailField(),
            passwordField(),
            submitField()
          ]
        )
      )
    );
  }

  Widget emailField(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Email',
        hintText: 'you@example.com'
      ),
    );
  }

  Widget passwordField(){
    return TextFormField(
      obscureText: false,
      decoration: InputDecoration(
        labelText: 'Contraseña',
        hintText: 'Contraseña'
      ),
    );
  }

  Widget submitField(){
    return RaisedButton(
      child: Text("Enviar"),
      onPressed: (){}
    );
  }
}