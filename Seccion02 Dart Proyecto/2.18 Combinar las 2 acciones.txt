//2.18 Combinar las 2 acciones
import 'dart:html';
import 'dart:collection'; //Librería para ListQueue

class Rosco {
  ListQueue<Pregunta> roscoPreguntas = ListQueue<Pregunta>(); //Se inicializa creando una instancia del LisQueue
  List<String> respondidas = [];
  List<String> pasadas = [];

  Rosco() {
    roscoPreguntas.addAll(RoscoApi().obtenerRoscos());
  }

  Pregunta obtenerPregunta(bool inicial) {
    if(inicial)
      return roscoPreguntas.first;
    
    //Función de firstwhere busca el primer elemento donde la letras de la colección roscoPreguntas sea diferente a las letras de la colección respondidas.
    //La función de any compara la letra de cada elemento de roscoPreguntas con todas las letras de la colección respondidas
    var siguientePregunta = roscoPreguntas.firstWhere((rosco) => !respondidas.any((x) => x == rosco.letra) && !pasadas.any((x) => x == rosco.letra), orElse: () => null);
    
    if(siguientePregunta == null){
      respondidas = [];
      return obtenerPregunta(false);
    }
    
    return siguientePregunta;
  }

  Pregunta pasapalabra(String letraActual) {
    var siguientePregunta = roscoPreguntas.firstWhere(
      (rosco) => !(rosco.letra == letraActual) && !pasadas.any((x) => x == rosco.letra) && !respondidas.any((x) => x == rosco.letra),
      orElse: () => null
    );
    
    if(siguientePregunta == null){
      pasadas = [];
      return pasapalabra("");
    }
    
    pasadas.add(letraActual);
    return siguientePregunta;
  }

  String evaluarRespuesta(String letra, String respuesta) {
    var pregunta = roscoPreguntas
        .firstWhere((rosco) => rosco.letra == letra);

    respondidas.add(pregunta.letra);
    
    print(respondidas);
    
    return pregunta.respuesta == respuesta
        ? "Letra $letra respuesta correcta"
        : "Letra $letra respuesta incorrecta";
  }
}

class Pregunta {
  String letra;
  String definicion;
  String respuesta;

  Pregunta(this.letra, this.definicion, this.respuesta);
}

class Db {
  static List letras = const ["A", "B", "C", "D", "E", "F"];
  static List definiciones = const [
    "Persona que tripula una Astronave o que está entrenada para este Trabajo",
    "Especie de Talega o Saco de Tela y otro material que sirve para llevar o guardar algo",
    "Aparato destinado a registrar imágenes animadas para el cine o la telivision",
    "Obra literaria escrita para ser representada",
    "Que se prolonga muchisimo o excesivamente",
    "Laboratorio y despacho del farmaceutico"
  ];
  static List respuestas = [
    "Astronauta",
    "Bolsa",
    "Camara",
    "Drama",
    "Eterno",
    "Farmacia"
  ];
}

class RoscoApi {
  List<Pregunta> roscoPreguntas = [];

  //Se puede acceder a los atributos de la clase Db sin instanciar la clase porque los atributos son estáticos.
  List<Pregunta> obtenerRoscos() {
    for (var letra in Db.letras) {
      var index = Db.letras.indexOf(letra);
      var roscoPregunta =
          Pregunta(letra, Db.definiciones[index], Db.respuestas[index]);
      roscoPreguntas.add(roscoPregunta);
    }
    return roscoPreguntas;
  }
}

void main() {
  var rosco = Rosco();
  var primeraDefinicion = rosco.obtenerPregunta(true);

  querySelector("#pregunta").text = primeraDefinicion.definicion;
  querySelector("#letra").text = primeraDefinicion.letra;

  querySelector("#btnEnviar").onClick.listen((event) {
    var respuesta = (querySelector("#textRespuesta") as InputElement)
        .value; //As para castear o convertir el elemento a input y obtener su valor.
    var letra = querySelector("#letra").text;

    String mensaje = rosco.evaluarRespuesta(letra, respuesta);

    //print(letra);
    //print(respuesta);

    var nuevaPregunta = rosco.obtenerPregunta(false);
    actualizarUI(nuevaPregunta);
    
    print(mensaje);
  });
  
  querySelector("#btnPasapalabra").onClick.listen((event) {
    var nuevaPregunta = rosco.pasapalabra(querySelector("#letra").text);
    actualizarUI(nuevaPregunta);
  });
}

void actualizarUI(Pregunta pregunta){
  querySelector("#letra").text = pregunta.letra;
  querySelector("#pregunta").text = pregunta.definicion;
  querySelector("#textRespuesta").text = "";
}