6.3 Proyecto stateless

import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: MyApp()
  ));
}

class MyApp extends StatelessWidget{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Stateless widget"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Card(
              child: Text("Card")
            )
          ],
        )
      )
    );
  }
}