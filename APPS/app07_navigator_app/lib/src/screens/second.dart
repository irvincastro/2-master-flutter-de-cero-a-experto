import 'package:flutter/material.dart';

class Second extends StatelessWidget {
  final String title;
  Second({this.title});

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text(title)
      ),
      body: Center(
        child: RaisedButton(
          child: Text("Ir Atrás"),
          onPressed: (){
            Navigator.pop(context);
          }
        )
      )
    );
  }
}