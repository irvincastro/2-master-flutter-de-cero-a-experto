import 'package:flutter/material.dart';

void main(){
  var app = MaterialApp(
    home: Scaffold(
        appBar: AppBar(
          title: Container(
            child: Center(
              child: Text("Mi primera aplicación"),
            ),
          ),
        ),
        body: Container(
          child: Center(
            child: Text(
              "Hello world",
              style: TextStyle(fontSize: 40),
            ),
          ), 
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: (){ print('click'); },
        ),
      )
  );

  runApp(app);
}