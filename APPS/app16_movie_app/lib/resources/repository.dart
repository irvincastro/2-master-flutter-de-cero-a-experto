import 'package:movie_app/common/MediaProvider.dart';
import 'package:movie_app/model/Cast.dart';
import 'package:movie_app/resources/api_provider.dart';
import 'package:movie_app/resources/db_provider.dart';
import 'package:movie_app/resources/resource_provider.dart';


class Repository{ //Clase que llamará Db o Api
  static final Repository _repository = Repository();
  //ApiProvider _apiProvider = ApiProvider.get();
  //DBProvider _dbProvider = DBProvider.get();
  List<ResourceProvider> _providers = <ResourceProvider>[
    dbProvider,
    apiProvider
  ];
  List<CacheProvider> caches = <CacheProvider>[
    dbProvider
  ];

  static Repository get(){
    return _repository;
  }

  Future<List<Cast>> fetchCasts(int mediaId, MediaType mediaType) async{
    var provider;
    List<Cast> list;

    for (provider in _providers) {
      list = await provider.fetchCasts(mediaId, mediaType);
      if(list != null){
        break;
      }
    }

    for (var cache in caches) {
      if(cache != provider){
        list.forEach((element) => cache.addCast(element));
      }
    }
    return list;
    // List<Cast> list = await _dbProvider.fetchCasts(mediaId);
    // if(list != null){
    //   return list;
    // }

    // list = await _apiProvider.fetchCreditMovies(mediaId);

    // list.forEach((element) => _dbProvider.addCast(element));
    
    // return list;
  }

}