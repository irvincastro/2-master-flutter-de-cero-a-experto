import 'package:flutter/material.dart';
import 'package:movie_app/common/HttpHandler.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadJson();
  }

  _loadJson() async{
    String data = await HttpHandler().fetchMovies();
    print(data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Movie"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.white), 
            onPressed: (){}
          )
        ],
      ),
      drawer: Drawer(
        child: new ListView(
          children: <Widget>[
            DrawerHeader(child: Material()),
            ListTile(
              title: Text("Peliculas"),
              trailing: Icon(Icons.local_movies),
            ),
            Divider(
              height: 5.0
            ),
            ListTile(
              title: Text("Teelevisión"),
              trailing: Icon(Icons.live_tv)
            ),
            Divider(
              height: 5.0
            ),
            ListTile(
              title: Text("Cerrar"),
              trailing: Icon(Icons.close),
              onTap: () => Navigator.of(context).pop() //Cierra rutas de la app
            )
          ],
        )
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: _getFooterItems()
      )
    );
  }
  List<BottomNavigationBarItem> _getFooterItems(){
      return [
        BottomNavigationBarItem(
          icon: Icon(Icons.thumb_up),
          title: Text("Populares")
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.update),
          title: Text("Próximamente")
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.star),
          title: Text("Mejor valoradas")
        ),
      ];
    }
}