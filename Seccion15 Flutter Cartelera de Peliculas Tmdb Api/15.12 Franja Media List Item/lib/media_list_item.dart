import 'package:flutter/material.dart';
import 'package:movie_app/model/Media.dart';

class MediaListItem extends StatelessWidget {
  final Media media;
  MediaListItem(this.media);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          Container(
            child: Stack(
              children: <Widget>[
                FadeInImage.assetNetwork(
                  placeholder: "assets/placeholder.jpg", 
                  image: media.getBackDropUrl(),
                  fit: BoxFit.cover, //La imagen se adapta centrada para cubrir todo el espacio de su contenedor
                  fadeInDuration: Duration(milliseconds: 40),
                  height: 200.0, //Indica la altura de la imagen
                  width: double.infinity, //Ocupa todo el ancho
                ),
                Positioned( //Widget para posicionar elemento en pantalla
                  left: 0.0, //Indica que habrá 0 espacio entre lado izq y elemento
                  bottom: 0.0,
                  right: 0.0,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[900].withOpacity(0.5)
                    ),
                    //Restricción de espacio que ocupará el elemento dentro del card
                    constraints: BoxConstraints.expand( 
                      height: 55.0
                    ),
                  )
                )
              ],
            ),
          )
          
        ],
      ),
    );
  }
}