import 'package:flutter/material.dart';
import 'package:movie_app/common/MediaProvider.dart';
import 'package:movie_app/model/Cast.dart';

class CastController extends StatefulWidget {
  final MediaProvider provider;
  final int mediaId;
  CastController(this.provider, this.mediaId);

  @override
  _CastControllerState createState() => _CastControllerState();
}

class _CastControllerState extends State<CastController> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadCasts();
  }

  final List<Cast> _casts = List<Cast>();
  void loadCasts() async{
    var results = await widget.provider.fetchCast(widget.mediaId);

    setState(() {
      _casts.addAll(results);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      
    );
  }
}