import 'package:flutter/material.dart';
import 'package:movie_app/model/Media.dart';

class MediaDetail extends StatelessWidget {
  final Media media;
  MediaDetail(this.media);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack( //Permite crear widgets posicionados sobrepuestos
        fit: StackFit.expand, //Usa todo el espacio de pantalla
        children: <Widget>[
          Image.network(
            media.getBackDropUrl(),
            fit: BoxFit.cover,
          )
        ],
      )
    );
  }
}