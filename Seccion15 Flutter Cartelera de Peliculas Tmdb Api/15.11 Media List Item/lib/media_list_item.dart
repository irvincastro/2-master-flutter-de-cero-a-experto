import 'package:flutter/material.dart';
import 'package:movie_app/model/Media.dart';

class MediaListItem extends StatelessWidget {
  final Media media;
  MediaListItem(this.media);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          FadeInImage.assetNetwork(
            placeholder: "assets/placeholder.jpg", 
            image: media.getBackDropUrl(),
            fit: BoxFit.cover, //La imagen se adapta centrada para cubrir todo el espacio de su contenedor
            fadeInDuration: Duration(milliseconds: 40),
            height: 200.0, //Indica la altura de la imagen
            width: double.infinity, //Ocupa todo el ancho
          )
        ],
      ),
    );
  }
}