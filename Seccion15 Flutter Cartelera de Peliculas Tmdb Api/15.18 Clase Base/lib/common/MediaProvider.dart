import 'dart:async';
import 'package:movie_app/model/Media.dart';
abstract class MediaProvider{

  Future<List<Media>> fetchMedia();
}