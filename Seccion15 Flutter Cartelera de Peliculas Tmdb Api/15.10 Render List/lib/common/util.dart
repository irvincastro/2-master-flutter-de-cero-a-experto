final String _imageUrlMedium = "https://image.tmdb.org/t/p/w300";
//https://developers.themoviedb.org/3/configuration/get-api-configuration
String getMediumPictureUrl(String path) => _imageUrlMedium + path;