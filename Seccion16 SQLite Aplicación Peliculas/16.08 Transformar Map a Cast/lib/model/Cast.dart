import 'package:movie_app/common/MediaProvider.dart';
import 'package:movie_app/common/util.dart';

class Cast{
  int id;
  String name;
  String profilePath;

  String getCastUrl() => getMediumPictureUrl(profilePath);

  factory Cast(Map jsonMap, MediaType mediaType){
    try {
      return Cast.deserialize(jsonMap, mediaType);
    }catch (ex) {
      throw ex;
    }
  }

  Cast.deserialize(Map jsonMap, MediaType mediaType) :
    id = (mediaType == MediaType.movie ? jsonMap["cast_id"] : jsonMap["id"]).toInt(),
    name = jsonMap["name"],
    //profilePath = jsonMap["profile_path" ?? "/eze9FO9VuryXLP0aF2cRqPCcIBN.jpg"];
    profilePath = jsonMap["profile_path"] ?? "assets/placeholder.png";

    Cast.fromDb(Map<String, dynamic> parsedJson) :
      id = parsedJson["id"].toInt(),
      name = parsedJson["name"],
      profilePath = parsedJson["profile_path"];
}