import 'package:movie_app/resources/api_provider.dart';
import 'package:movie_app/resources/db_provider.dart';

class Repository{ //Clase que llamará Db o Api
  ApiProvider _apiProvider = ApiProvider.get();
  DBProvider _dbProvider = DBProvider.get();
}