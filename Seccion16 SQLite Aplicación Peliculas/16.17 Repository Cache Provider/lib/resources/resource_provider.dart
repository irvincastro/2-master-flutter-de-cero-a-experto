import 'package:movie_app/common/MediaProvider.dart';
import 'package:movie_app/model/Cast.dart';

//DECLARACIONES DE 2 INTERFACES
//Interfaz con el método fetchCasts implementado en DBProvider y ApiProvider
abstract class ResourceProvider{
  Future<List<Cast>> fetchCasts(int mediaId, MediaType mediaType);
}

//Interfaz con el método addCast implementado en DBProvider
abstract class CacheProvider{
  void addCast(Cast cast);
}