import 'package:movie_app/model/Cast.dart';
import 'package:movie_app/resources/api_provider.dart';
import 'package:movie_app/resources/db_provider.dart';
import 'package:movie_app/resources/resource_provider.dart';


class Repository{ //Clase que llamará Db o Api
  static final Repository _repository = Repository();
  //ApiProvider _apiProvider = ApiProvider.get();
  //DBProvider _dbProvider = DBProvider.get();
  List<ResourceProvider> _providers = <ResourceProvider>[
    dbProvider,
    apiProvider
  ];

  static Repository get(){
    return _repository;
  }

  Future<List<Cast>> fetchCastMovies(int mediaId) async{
    List<Cast> list = await _dbProvider.fetchCasts(mediaId);
    if(list != null){
      return list;
    }

    list = await _apiProvider.fetchCreditMovies(mediaId);

    list.forEach((element) => _dbProvider.addCast(element));
    
    return list;
  }

  Future<List<Cast>> fetchCastShows(int mediaId) async{
    List<Cast> list = await _dbProvider.fetchCasts(mediaId);
    if(list != null){
      return list;
    }
    list = await _apiProvider.fetchCreditShows(mediaId);
    list.forEach((element) => _dbProvider.addCast(element));
    return list;
  }
}