import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:movie_app/model/Cast.dart';

class DBProvider{
  static final DBProvider _dbProvider = DBProvider();
  Database db; //Conexion con base de datos
  //En un constructor no se pueden realizar llamadas asíncronas
  DBProvider(){ //Método constructor
    init(); 
  }

  static DBProvider get(){
    return _dbProvider;
  }

  //MÉTODO PARA CREAR O ACCEDER A LA BD
  void init()async{
    Directory documentDirecory = await getApplicationDocumentsDirectory(); //Obtiene ruta segura para almacenar la bd
    final path = join(documentDirecory.path, "Casts4.db"); //Ruta concatenada con el nombre de la bd.
    db = await openDatabase( //La primera vez se crea, las siguientes se accede a la bd
      path, //Esta es la ruta donde se crea o accede la bd.
      version: 1, //Por si se modifica la estructura de la bd
      onCreate: (Database newDb, int version){ //Crea la estructura principal de la bd. Se llama este método sólo la primera vez que se accede a la bd.
        //Execute(), ejecuta una sentencia sql. Con comillas triples se puede crear string multilinea.
        newDb.execute("""
          CREATE TABLE Casts
          (
            id INTEGER PRIMARY KEY,
            name TEXT,
            profile_path TEXT,
            media_Id INTEGER
          )
        """);
      }
    );
  }

  //MÉTODO PARA OBTENER DATOS
  Future<List<Cast>> fetchCasts(int mediaId) async{
    print('${mediaId.toString()} Lectura de Base de datos Local');
    var maps = await db.query(
      "Casts",
      columns: null, //Trae todas las columnas
      where: "media_Id = ?",
      whereArgs: [mediaId] //Where en dos lineas es más seguro, evita sql inyection a db
    );

    if(maps.length > 0){
      return maps.map<Cast>((item) => Cast.fromDb(item)).toList();
    }

    return null;
  }

  //Esta no es async porque no se espera un resultado
  //MÉTODO PARA AGREGAR DATOS
  void addCast(Cast cast){
    print('${cast.mediaId.toString()} Insertar en Base de datos Local');
    db.insert(
      "Casts",
      cast.toMap(),
      conflictAlgorithm: ConflictAlgorithm.fail
    );
  }

}