import 'package:flutter/material.dart';
import 'package:movie_app/media_list.dart';
import 'package:movie_app/common/MediaProvider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  void initState() {
    // TODO: implement initState
    _pageController = PageController();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController.dispose();
    super.dispose();
  }

  final MediaProvider movieProvider = MovieProvider();
  final MediaProvider showProvider = ShowProvider();
  PageController _pageController;
  int _page = 0;
  MediaType mediaType = MediaType.movie;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Movie"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.white), 
            onPressed: (){}
          )
        ],
      ),
      drawer: Drawer(
        child: new ListView(
          children: <Widget>[
            DrawerHeader(child: Material()),
            ListTile(
              title: Text("Peliculas"),
              selected: mediaType == MediaType.movie,
              trailing: Icon(Icons.local_movies),
              onTap: (){
                _changeMediaType(MediaType.movie);
                Navigator.of(context).pop();
              }
            ),
            Divider(
              height: 5.0
            ),
            ListTile(
              title: Text("Televisión"),
              selected: mediaType == MediaType.show,
              trailing: Icon(Icons.live_tv),
              onTap: (){
                _changeMediaType(MediaType.show);
                Navigator.of(context).pop();
              }
            ),
            Divider(
              height: 5.0
            ),
            ListTile(
              title: Text("Cerrar"),
              trailing: Icon(Icons.close),
              onTap: () => Navigator.of(context).pop() //Cierra rutas de la app
            )
          ],
        )
      ),
      body: PageView(
        children: _getMediaList(),
        controller: _pageController,
        onPageChanged: (int index){
          setState(() {
            _page = index;
          });
        }
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: _getFooterItems(),
        onTap: _navigationTapped,
        currentIndex: _page,
      )
    );
  }
  List<BottomNavigationBarItem> _getFooterItems(){
      return mediaType == MediaType.movie ?
      [
        BottomNavigationBarItem(
          icon: Icon(Icons.thumb_up),
          title: Text("Populares")
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.update),
          title: Text("Próximamente")
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.star),
          title: Text("Mejor valoradas")
        ),
      ]:
      [
        BottomNavigationBarItem(
          icon: Icon(Icons.thumb_up),
          title: Text("Populares")
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.update),
          title: Text("En el aire")
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.star),
          title: Text("Mejor valoradas")
        ),
      ];
    }

    void _changeMediaType(MediaType type){
      if(mediaType != type){
        setState(() {
          mediaType = type;
        });
      }
    }

    List<Widget> _getMediaList(){
      return (mediaType == MediaType.movie) ?
      <Widget>[
        MediaList(movieProvider, "popular"),
        MediaList(movieProvider, "upcoming"),
        MediaList(movieProvider, "top_rated"),
      ]:
      <Widget>[
        MediaList(showProvider, "popular"),
        MediaList(showProvider, "on_the_air"),
        MediaList(showProvider, "top_rated"),
      ];
    }

    void _navigationTapped(int page){
      _pageController.animateToPage(
        page, 
        duration: const Duration(microseconds: 300), 
        curve: Curves.ease);
    }
}