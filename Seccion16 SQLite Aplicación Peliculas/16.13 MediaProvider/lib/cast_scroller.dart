import 'package:flutter/material.dart';
import 'package:movie_app/common/MediaProvider.dart';
import 'package:movie_app/model/Cast.dart';

class CastController extends StatefulWidget {
  final MediaProvider provider;
  final int mediaId;
  CastController(this.provider, this.mediaId);

  @override
  _CastControllerState createState() => _CastControllerState();
}

class _CastControllerState extends State<CastController> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadCasts();
  }

  final List<Cast> _casts = List<Cast>();
  void loadCasts() async{
    var results = await widget.provider.fetchCast(widget.mediaId);

    setState(() {
      _casts.addAll(results);
    });
  }

  Widget _builderCasts(BuildContext ctx, int index){
    var cast = _casts[index];
    return Padding(
      padding: const EdgeInsets.only(right: 16.0),
      child: Column(
        children: <Widget>[
          CircleAvatar(
            backgroundImage: NetworkImage(
              //"assets/placeholder.png"
              //cast.getCastUrl() == null ? "assets/placeholder.png" : cast.getCastUrl() 
              cast.getCastUrl() == "null" ? "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ_hp5h8c5u37qsJdeLl_iqlq8LfF8YrhuLaw&usqp=CAU" : cast.getCastUrl() 
              //"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ_hp5h8c5u37qsJdeLl_iqlq8LfF8YrhuLaw&usqp=CAU"
            ),
            radius: 40.0,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(cast.name),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox.fromSize(
          size: const Size.fromHeight(180.0),
          child: ListView.builder(
            itemCount: _casts.length,
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.only(top: 12.0, left: 20.0),
            itemBuilder: _builderCasts
          )
        )
      ],
    );
  }
}