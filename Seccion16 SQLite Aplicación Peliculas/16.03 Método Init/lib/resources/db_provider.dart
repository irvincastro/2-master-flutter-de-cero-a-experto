import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'dart:io';

class DBProvider{
  Database db; //Conexion con base de datos
  //En un constructor no se pueden realizar llamadas asíncronas
  DBProvider(){ //Método constructor
    init(); 
  }
  void init(){
    Directory documentDirecory = await getApplicationDocumentsDirectory(); //Obtiene ruta segura para almacenar la bd
    final path = join(documentDirecory.path, "Casts4.db"); //Ruta concatenada con el nombre de la bd.
  }

}