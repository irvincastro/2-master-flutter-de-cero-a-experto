import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'dart:io';

class DBProvider{
  Database db; //Conexion con base de datos
  //En un constructor no se pueden realizar llamadas asíncronas
  DBProvider(){ //Método constructor
    init(); 
  }
  void init(){
    Directory documentDirecory = await getApplicationDocumentsDirectory(); //Obtiene ruta segura para almacenar la bd
    final path = join(documentDirecory.path, "Casts4.db"); //Ruta concatenada con el nombre de la bd.
    db = await openDatabase( //La primera vez se crea, las siguientes se accede a la bd
      path, //Esta es la ruta donde se crea o accede la bd.
      version: 1, //Por si se modifica la estructura de la bd
      onCreate: (Database newDb, int version){ //Crea la estructura principal de la bd. Se llama este método sólo la primera vez que se accede a la bd.
        //Execute(), ejecuta una sentencia sql. Con comillas triples se puede crear string multilinea.
        newDb.execute("""
          CREATE TABLE Casts
          (
            id INTEGER PRIMARY KEY,
            name TEXT,
            profile_path TEXT,
            mediaId INTEGER
          )
        """);
      }
    );
  }

}