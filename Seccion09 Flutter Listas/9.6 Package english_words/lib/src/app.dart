import 'package:flutter/material.dart';
import 'dart:math';
import 'package:english_words/english_words.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => new _AppState();
 }
class _AppState extends State<App> {
  final suggestions = <WordPair>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista infinita")
      ),
      body: ListView.builder(
        itemBuilder: (context, i){

          if(i >= suggestions.length){
            suggestions.addAll(generateWordPairs().take(10));
          }          

          return ListTile(
            leading: Icon(Icons.shopping_cart),
            title: Text('producto $i'),
            subtitle: Text('precio: ${Random().nextInt(100)} USD')
          );
        }
      )
    );
  }
}