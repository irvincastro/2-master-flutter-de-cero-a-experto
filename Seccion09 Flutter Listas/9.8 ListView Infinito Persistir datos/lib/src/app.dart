import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => new _AppState();
 }
class _AppState extends State<App> {
  final suggestions = <WordPair>[];
  final saved = <WordPair>[];

  ListTile buildRow(WordPair pair){
    return ListTile(
            trailing: Icon(Icons.favorite),
            title: Text(pair.asPascalCase),
            onTap: (){
              setState(() {
                saved.add(pair);
              });
            }
          );
  }

  

  @override
  Widget build(BuildContext context) {

    //Creación de nueva página dentro de este método, sin crear un nuevo archivo.
    void pushSaved(){
      Navigator.push(
        context,
        MaterialPageRoute( builder: (context){

          final tiles = saved.map<ListTile>((pair) => ListTile(title: Text(pair.asCamelCase))).toList();

          return Scaffold(
            appBar: AppBar(
              title: Text("Guardadas")
            ),
            body: ListView(children: tiles)
          );
        }
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Lista infinita"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.list), onPressed: pushSaved)
        ]
      ),
      body: ListView.builder(
        itemBuilder: (context, i){

          if(i.isOdd) return Divider();

          if(i >= suggestions.length){
            suggestions.addAll(generateWordPairs().take(10));
          }          

          return buildRow(suggestions[i]);
        }
      )
    );
  }
}