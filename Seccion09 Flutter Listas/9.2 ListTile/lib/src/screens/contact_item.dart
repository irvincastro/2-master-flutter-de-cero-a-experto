import 'package:flutter/material.dart';

class ContactItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(child: Text("I")),
      title: Text("Irvin"),
      subtitle: Text("irvin_gama@hotmail.com")
    );
  }
}