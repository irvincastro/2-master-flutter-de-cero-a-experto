import 'package:flutter/material.dart';
import 'package:listview_app/src/screens/contact_item.dart';

class Contact extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          child: Center(
            child: Text("Contactos")
          )
        )
      ),
      body: ListView(
        children: <Widget>[
          ContactItem()
        ]
      )
    );
  }
}