import 'package:flutter/material.dart';
import 'package:listview_app/src/model/contact_model.dart';
import 'package:listview_app/src/screens/contact_item.dart';

class Contact extends StatelessWidget {

  buildList(){
    return <ContactModel>[
      //Parámetros nombrados:
      ContactModel(name: "Irvin Castro", email: "irvin@gmail.com"),
      ContactModel(name: "Rogrigo Morales", email: "rodrigo@gmail.com"),
      ContactModel(name: "Martin Fowler", email: "martin.fowler@gmail.com"),
      ContactModel(name: "Linux tolvar", email: "linux.tolvar@gmail.com")
      ];
  }

  List<ContactItem> buildContactList(){
    return buildList()
      .map<ContactItem>((contact) => ContactItem(contact: contact))
      .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          child: Center(
            child: Text("Contactos")
          )
        )
      ),
      body: ListView(
        children: buildContactList()
      )
    );
  }
}