import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      //Un builder puede servir para leer páginas de un API externo y cargalas en la app.
      child: PageView.builder(
        itemBuilder: (context, index){
          return Container(
            color: index % 2 == 0 ? Colors.pink : Colors.cyan   
          );
        }
      )
    );
  }
}