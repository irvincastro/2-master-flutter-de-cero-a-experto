import 'package:flutter/material.dart';

class Second extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Segunda pantalla")
      ),
      body: Center(
        child: RaisedButton(
          child: Text("Ir Atrás"),
          onPressed: (){
            Navigator.pop(context);
          }
        )
      )
    );
  }
}