import 'package:flutter/material.dart';
import 'package:login_bloc/src/bloc/bloc.dart';


class Provider extends InheritedWidget{
  //El guión bajo indica que no se hará nada con el parámetro
  bool updateShouldNotify(_) => true;

  final bloc = Bloc();

  static Bloc of (BuildContext context) =>
    //Busca context de tipo Prodiver para dar acceso al BLOC desde cualquier widget de la jerarquía
    (context.inheritFromWidgetOfExactType(Provider) as Provider).bloc;
}