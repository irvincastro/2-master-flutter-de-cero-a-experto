import 'package:flutter/material.dart';
import 'package:login_bloc/src/bloc/bloc.dart';
import 'package:provider/provider.dart';
import 'package:login_bloc/src/screens/login_screen.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<Bloc>(
      //Forma antigua de poner parámetros
      //builder: (context) => Bloc(), 
      //dispose: (context, bloc) => bloc.dispose(),
      //FORMA ACTUAL:
      create: (_) => Bloc(),
      dispose: (context, bloc) => bloc.dispose(),
      child:
        MaterialApp(
          home: Scaffold(
            body: LoginScreen()
          )
        )
    );
  }
}