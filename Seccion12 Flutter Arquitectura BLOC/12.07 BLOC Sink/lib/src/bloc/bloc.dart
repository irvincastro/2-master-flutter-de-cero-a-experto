import 'dart:async';

class Bloc{
  //Guión bajo para que sean privados y no públicos. Y sólo haya acceso a los métodos y no a las propiedades directamente.
  final _emailController = StreamController<String>();
  final _passwordController = StreamController<String>();

  Function(String) get changeEmail => emailController.sink.add;
  Function(String) get changePassword => passwordController.sink.add;
}