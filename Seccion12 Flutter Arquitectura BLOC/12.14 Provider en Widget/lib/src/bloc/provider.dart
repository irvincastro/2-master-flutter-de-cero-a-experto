import 'package:flutter/material.dart';
import 'package:login_bloc/src/bloc/bloc.dart';


class Provider extends InheritedWidget{

   //Constructor para envolver el Widget login_screen dentro de Provider
   Provider({Key key, Widget child}) : super(key: key, child: child);

  final bloc = Bloc();

  //El guión bajo indica que no se hará nada con el parámetro
  bool updateShouldNotify(_) => true;

  static Bloc of (BuildContext context) =>
    //Busca context de tipo Prodiver para dar acceso al BLOC desde cualquier widget de la jerarquía
    (context.inheritFromWidgetOfExactType(Provider) as Provider).bloc;
}