import 'dart:async';
import 'package:login_bloc/src/bloc/validators.dart';
import 'package:rxdart/rxdart.dart';

class Bloc with Validators{
  //Guión bajo para que sean privados y no públicos. Y sólo haya acceso a los métodos y no a las propiedades directamente.
  final _emailController = StreamController<String>.broadcast();
  final _passwordController = StreamController<String>.broadcast();

  Stream<String> get email => _emailController.stream.transform(validaEmail);
  Stream<String> get password => _passwordController.stream.transform(validaPassword);

  Stream<bool> get submitValid =>
  Rx.combineLatest2(email, password, (a, b) => true);

  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;

  //Para liberar recursos del stream y evitar problemas en la app.
  dispose(){
    _emailController.close();
    _passwordController.close();
  }
}