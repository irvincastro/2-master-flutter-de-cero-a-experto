import 'dart:async';
import 'package:login_bloc/src/bloc/validators.dart';
import 'package:rxdart/rxdart.dart';

class Bloc with Validators{
  //Guión bajo para que sean privados y no públicos. Y sólo haya acceso a los métodos y no a las propiedades directamente.
  final _emailController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();

  Stream<String> get email => _emailController.stream.transform(validaEmail);
  Stream<String> get password => _passwordController.stream.transform(validaPassword);

  Stream<bool> get submitValid =>
  Rx.combineLatest2(email, password, (a, b) => true);

  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;

  submit(){
    final validEmail = _emailController.value;
    final validPassword = _passwordController.value;

    print('Email $validEmail');
    print('Password $validPassword');
  }

  //Para liberar recursos del stream y evitar problemas en la app.
  dispose(){
    _emailController.close();
    _passwordController.close();
  }
}