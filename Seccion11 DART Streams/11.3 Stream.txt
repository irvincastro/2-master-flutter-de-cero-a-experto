11.3 Stream

CÓDIGO DE DARTPAD:
//11.3 Stream
import 'dart:async'

void main(){
  final order = Order('sandwich');
  final controller = StreamController(); //Proviene de la librería
  controller.sink.add(order); //Diego está mandando la orden al restaurante
  controller.stream.map((order) => order.type); //Recepcionista toma el dato importante de la orden
}

class Order{
  String type;
  Order(this.type);
}

class Food{
  
}